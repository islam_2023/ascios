//
//  UITextFields.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/19/20.
//  Copyright © 2020 islam. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import RAGTextField
import UIKit


enum BookFields {
    case name
    case mobile
    case email
    
    func leftImage()->UIImage? {
        switch self {
        case .name:
            return #imageLiteral(resourceName: "name")
        case .mobile:
            return #imageLiteral(resourceName: "mobile")
           case .email:
            return #imageLiteral(resourceName: "email_book")
        }
    }
    
    func error()->String {
        switch self {
        case .name:
            return L.nameError.Locaz
        case .mobile:
            return L.mobileError.Locaz
        case .email:
            return L.emailError.Locaz
        }
    }
}

class ASCTextField :RAGTextField ,UITextFieldDelegate{
    
    private let disposeBag = DisposeBag()
    var field:BookFields = .name
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupTextField(field: field)
        
    }
    
    func setupTextField(field:BookFields){
        self.field = field
        self.delegate = self
        let bgView = UnderlineView(frame: .zero)
        bgView.textField = self
        bgView.backgroundLineColor = ColorPalette.cloudyBlue
        bgView.foregroundLineColor = ColorPalette.cloudyBlue
        bgView.foregroundLineWidth = 2.0
        bgView.expandDuration = 0.2
        bgView.backgroundColor = ColorPalette.backgrou
        
        //        if #available(iOS 11, *) {
        bgView.layer.cornerRadius = 8.0
        bgView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.textColor = .black
        self.tintColor = .black
        self.textBackgroundView = bgView
        self.textPadding = UIEdgeInsets(top: 12.0, left: 12.0, bottom: 12.0, right: 12.0)
        self.textPaddingMode = .textAndPlaceholder
        self.scaledPlaceholderOffset = 0.0
        self.placeholderMode = .scalesWhenEditing
        self.placeholderColor = ColorPalette.brownGrey
        self.transformedPlaceholderColor = ColorPalette.windowsBlue
        self.placeholderFont = Fonts.SegoUI.bold(of: 12)
        self.placeholderScaleWhenEditing = 1
        self.hintFont = Fonts.SegoUI.regular(of: 11)
        self.hintColor = ColorPalette.rouge
        self.hintOffset = 3.0
        self.hint = ""
        self.font = Fonts.SegoUI.regular(of: 14)
        //        }
        if let image = field.leftImage() {
            self.leftView = UIImageView(image: image)
            self.leftViewMode = .always
        }
        
        validateData()
    }
    
    
    func validateData(){
        self.rx.text.bind { [weak self] (rxText) in
            guard let rxText = rxText , rxText.count > 0 else {return}
            guard let strongSelf = self else {return}
            switch strongSelf.field {
            case .name:
                if rxText.validateName {
                    strongSelf.hint = nil
                }else {
                    strongSelf.hint = strongSelf.field.error()
                }
                
            case .mobile:
                if rxText.validateMobile {
                    strongSelf.hint = nil
                }else {
                    strongSelf.hint = strongSelf.field.error()
                }
                
            case .email:
                if rxText.validateEmail {
                    strongSelf.hint = nil
                }else {
                    strongSelf.hint = strongSelf.field.error()
                }
            }
        }.disposed(by: disposeBag)
    }
}

