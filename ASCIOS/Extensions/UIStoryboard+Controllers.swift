//
//  UIStoryboard+Controllers.swift
//  MovieApp
//
//  Created by Anshul Shah on 12/11/18.
//  Copyright © 2018 Anshul Shah. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    static var main: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    static var AlertScreens: UIStoryboard {
           return UIStoryboard(name: "AlertScreens", bundle: nil)
       }
    
}

extension UIStoryboard {
    var homeViewController: OffersViewController {
        guard let viewController = instantiateViewController(withIdentifier: String(describing: OffersViewController.self)) as? OffersViewController else {
            fatalError(String(describing: OffersViewController.self) + "\(NSLocalizedString("couldn't be found in Storyboard file", comment: ""))")
        }
        return viewController
    }
    var paymentConfirmtionViewController: PaymentConfirmtionViewController {
        guard let viewController = instantiateViewController(withIdentifier: String(describing: PaymentConfirmtionViewController.self)) as? PaymentConfirmtionViewController else {
            fatalError(String(describing: PaymentConfirmtionViewController.self) + "\(NSLocalizedString("couldn't be found in Storyboard file", comment: ""))")
        }
        return viewController
    }
    
    var settingViewController: SettingViewController {
        guard let viewController = instantiateViewController(withIdentifier: String(describing: SettingViewController.self)) as? SettingViewController else {
            fatalError(String(describing: SettingViewController.self) + "\(NSLocalizedString("couldn't be found in Storyboard file", comment: ""))")
        }
        return viewController
    }
    
    var subMenuViewController: SubMenuViewController {
        guard let viewController = instantiateViewController(withIdentifier: String(describing: SubMenuViewController.self)) as? SubMenuViewController else {
            fatalError(String(describing: SettingViewController.self) + "\(NSLocalizedString("couldn't be found in Storyboard file", comment: ""))")
        }
        return viewController
    }
    
    var TAProfileVC: TAProfileViewController {
           guard let viewController = instantiateViewController(withIdentifier: String(describing: TAProfileViewController.self)) as? TAProfileViewController else {
               fatalError(String(describing: TAProfileViewController.self) + "\(NSLocalizedString("couldn't be found in Storyboard file", comment: ""))")
           }
           return viewController
       }
    var offerDetailsViewController: OfferDetailsViewController {
        guard let viewController = instantiateViewController(withIdentifier: String(describing: OfferDetailsViewController.self)) as? OfferDetailsViewController else {
            fatalError(String(describing: OfferDetailsViewController.self) + "\(NSLocalizedString("couldn't be found in Storyboard file", comment: ""))")
        }
        return viewController
    }
    
    var bookViewController:BookViewController {
              guard let viewController = instantiateViewController(withIdentifier: String(describing: BookViewController.self)) as? BookViewController else {
                  fatalError(String(describing: BookViewController.self) + "\(NSLocalizedString("couldn't be found in Storyboard file", comment: ""))")
              }
              return viewController
          }
    
}
