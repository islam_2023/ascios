//
//  Localizer.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/13/20.
//  Copyright © 2020 islam. All rights reserved.
//

import Foundation


enum Languages :String {
    case en
    case ar
}

class Localizer {
    
    private init() {}
    
    static func localizeAppTo(language:Languages) {
        MOLH.shared.activate(true)
        MOLH.setLanguageTo(language.rawValue)
        MOLHLanguage.setDefaultLanguage(language.rawValue)
        MOLH.reset()
    }
    
    static func save(langugae :Languages) {
        let isLanguageArabic = "isLanguageArabic"
        UserDefaults.standard.set(langugae == .ar ? true : false, forKey: isLanguageArabic)
    }
    
    static func setLang() {
//        if UserDefaults.isFirstLaunch() {
//            save(langugae: .ar)
//            localizeAppTo(language: .ar)
//        }else {
//            localizeAppTo(language: UserDefaults.isArabic() ? .ar : .en)
//        }
  
        save(langugae: .ar)
        localizeAppTo(language: .ar)
        
    }
    
}


extension UserDefaults {
    // check for is first launch - only true on first invocation after app install, false on all further invocations
    // Note: Store this value in AppDelegate if you have multiple places where you are checking for this flag
    static func isFirstLaunch() -> Bool {
        let hasBeenLaunchedBeforeFlag = "hasBeenLaunchedBeforeFlag"
        let isFirstLaunch = !UserDefaults.standard.bool(forKey: hasBeenLaunchedBeforeFlag)
        if (isFirstLaunch) {
            UserDefaults.standard.set(true, forKey: hasBeenLaunchedBeforeFlag)
            UserDefaults.standard.synchronize()
        }
        return isFirstLaunch
    }
    
    static func isArabic() -> Bool {
        let isLanguageArabic = "isLanguageArabic"
        let isArabicLang = UserDefaults.standard.bool(forKey: isLanguageArabic)
        return isArabicLang
    }
    
}
