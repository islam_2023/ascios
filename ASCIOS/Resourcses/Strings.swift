//
//  Strings.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/13/20.
//  Copyright © 2020 islam. All rights reserved.
//

import Foundation

enum L :String {
    
    case home
    case favs
    case search
    case more
    
    case about
    case contact
    case terms
    case privacy
    
    case emailError
    case mobileError
    case nameError
    
    
    var Locaz:  String{
        return self.rawValue.localized
    }
}


