//
//  ViewController.swift
//  ASCIOS
//
//  Created by islam on 2/4/20.
//  Copyright © 2020 islam. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
class BaseViewController: UIViewController {
    
    let disposeBag = DisposeBag()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
      if #available(iOS 13.0, *) {
        return .darkContent
      }
      return .default
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupUI()
    }
    func setupUI()  {
        
    }
    override func didReceiveMemoryWarning() {
           super.didReceiveMemoryWarning()
           // Dispose of any resources that can be recreated.
       }
    
    deinit {
        plog("deinit VC:\(self)")
    }

}


