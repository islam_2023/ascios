//
//  TabBarController.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/13/20.
//  Copyright © 2020 islam. All rights reserved.
//

import Foundation
import UIKit

enum HomeBarKind: Int {
    case offers
    case search
    case favs
    case more
    
    var title: String? {
        switch self {
        case .offers:
            return ""//L.home.Locaz
        case .search:
            return ""//L.search.Locaz
        case.favs:
            return ""//L.favs.Locaz
        case .more:
            return ""//L.more.Locaz
        }
    }
    
    var icon: UIImage? {
        switch self {
            
        case .offers:
            return UIImage(named: "explore")
        case .search:
            return UIImage(named: "search")
        case .favs:
            return UIImage(named: "favorite")
        case .more:
            return UIImage(named: "menu")
        @unknown default:
            return nil
        }
    }
    
    var navigationController: UINavigationController {
        let navigation = UINavigationController()
        navigation.tabBarItem.title = self.title
        navigation.tabBarItem.image = self.icon
        return navigation
    }
}


extension RawRepresentable where RawValue == Int {
    
    static var itemCount: Int {
        var index = 0
        while Self(rawValue: index) != nil {
            index += 1
        }
        
        return index
    }
    
    static var items: [Self] {
        var items = [Self]()
        var index = 0
        while let item = Self(rawValue: index) {
            items.append(item)
            index += 1
        }
        
        return items
    }
}
