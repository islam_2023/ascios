//
//  BookModel.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/19/20.
//  Copyright © 2020 islam. All rights reserved.
//

import Foundation

class BookModel {
    var name:String
    var mobile:String
    var email:String?
    var adults :Int = 1
    var children :Int = 0
    var currency_id:Int
    var offer_id :Int
    var offer_price:Double
    
    var totalCosts:Double {
        return self.offer_price * Double((adults + children))
    }
    var stringCosts :String {
        return "\(totalCosts) KWD"
    }
   
    
    
    init(offer_id :Int,offer_price :Double,name:String,mobile:String,currency_id:Int) {
        self.offer_id = offer_id
        self.offer_price = offer_price
        
        self.name = name
        self.mobile = mobile
        self.currency_id = currency_id
    }
}
