//
//  BookViewModel.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/19/20.
//  Copyright © 2020 islam. All rights reserved.
//

import Foundation
import RxSwift

class BookViewModel  {
    
    var data = Observable.just([BookModel(offer_id: 0, offer_price: 0, name: "", mobile: "", currency_id: 0)])
    
    private let dependencies:Dependencies
    
    init(dependencies:Dependencies) {
        self.dependencies = dependencies
    }
        
}
