//
//  BookViewController.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/19/20.
//  Copyright © 2020 islam. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BookViewController: BaseViewController {

    @IBOutlet weak var tableView:UITableView!

    var viewModel:BookViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func setupUI() {
        tableView.register(UINib(nibName:"BookTCell",bundle:nil), forCellReuseIdentifier: "BookTCell")

        viewModel.data.bind(to: tableView.rx.items(cellIdentifier: "BookTCell", cellType: BookTCell.self)) { [weak self](_, element, cell) in
            guard let strongSelf = self else {return}
           cell.data = element
            cell.backButton.rx.tap.bind { [weak self] in
                strongSelf.navigationController?.popViewController(animated: true)
            }
            cell.payButton.rx.tap.bind { [weak self] in

                self?.viewModel.data.asObservable().bind(onNext: { (models) in
                    models.map { (model) in
                        print(model)
                    }
                })
            }
        }.disposed(by: disposeBag)
        
    }
    
}

