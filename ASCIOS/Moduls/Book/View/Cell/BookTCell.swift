//
//  BookTCell.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/19/20.
//  Copyright © 2020 islam. All rights reserved.
//

import UIKit
import RAGTextField


class BookTCell: UITableViewCell ,UITextFieldDelegate{
    
    enum CountActions:Int {
        case adultPlus = 0
        case adultMinus = 1
        case childPlus = 3
        case childMinus = 4
    }
    
    @IBOutlet private weak var nameTF: ASCTextField!
        {
        didSet {
            self.nameTF.field = .name
        }
    }
    @IBOutlet private weak var mobileTF: ASCTextField!
        {
        didSet {
            self.mobileTF.field = .mobile
        }
    }
    @IBOutlet private weak var emailTF: ASCTextField!
        {
        didSet {
            self.emailTF.field = .email
        }
    }
    @IBOutlet private weak var childsCountLbl: UILabel!
    @IBOutlet private weak var adultsCountLbl: UILabel!
    @IBOutlet private weak var totoalCoastsLbl: UILabel!
    weak var data:BookModel! {
        didSet {
            guard let data = data else {return}
            self.childsCountLbl.text = "\(data.children)"
            self.adultsCountLbl.text = "\(data.adults)"
            self.totoalCoastsLbl.text = data.stringCosts
        }
    }
    
    @IBOutlet weak var payButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    
    @IBAction func countActionsHandler(_ sender:UIButton) {
        var editedData = self.data
        if sender.tag == 0{
            editedData?.adults += 1
//            self.adultsCountLbl.text = "\(data.adults)"
        }
        else if sender.tag == 1{
            if data.adults <= 1 {return}
            editedData?.adults -= 1
//            self.adultsCountLbl.text = "\(data.adults)"
        }
        else if sender.tag == 2{
            editedData?.children += 1
//            self.childsCountLbl.text = "\(data.children)"
        }
        else if sender.tag == 3{
            if let child = editedData?.children , child <= 0 {return}
            self.data.children -= 1
//            self.childsCountLbl.text = "\(data.children)"
        }
        
        
        self.data = editedData
    }
    
   
}
