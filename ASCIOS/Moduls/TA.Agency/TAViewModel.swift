//
//  TAViewModel.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/17/20.
//  Copyright © 2020 islam. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class TAViewModel {
    
    private let dispose = DisposeBag()
    private let dependencies:Dependencies
    
    let items = Observable.just(
        (0..<3).map { "\($0)" }
    )
    
    init(dependencies:Dependencies) {
        self.dependencies = dependencies
    }
    
    
}
