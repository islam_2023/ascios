//
//  TAPreviewCell.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/17/20.
//  Copyright © 2020 islam. All rights reserved.
//

import UIKit

class TAPreviewCell: UITableViewCell {

    @IBOutlet weak var imageV:UIImageView!
    @IBOutlet weak var backButt:UIButton!

    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageV.image = nil
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func previewImage(for image:UIImage) {
        self.imageV.image = image
    }
}
