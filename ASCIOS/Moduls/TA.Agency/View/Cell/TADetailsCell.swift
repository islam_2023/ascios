//
//  TADetailsCell.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/17/20.
//  Copyright © 2020 islam. All rights reserved.
//

import UIKit

class TADetailsCell: UITableViewCell {

    @IBOutlet weak var phoneLbl:UILabel!
    @IBOutlet weak var emailLbl:UILabel!
    @IBOutlet weak var addressLbl:UILabel!
    @IBOutlet weak var workingHoursLbl:UILabel!

    @IBOutlet weak var checkOffersButt:UIButton!

    @IBOutlet weak var callButt:UIButton!
    @IBOutlet weak var emailButt:UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(){
        
    }
}
