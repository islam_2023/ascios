//
//  TADescCell.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/17/20.
//  Copyright © 2020 islam. All rights reserved.
//

import UIKit

class TADescCell: UITableViewCell {

    @IBOutlet weak var titleLbl:UILabel!
    @IBOutlet weak var descLbl:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(title:String,desc:String ){
        self.titleLbl.text = title
        self.descLbl.text = desc
    }
}
