//
//  TAProfileViewController.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/17/20.
//  Copyright © 2020 islam. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class TAProfileViewController: BaseViewController ,UITableViewDelegate{
    
    @IBOutlet weak var tableView:UITableView!
    
    var viewModel : TAViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func setupUI() {
        tableView.register(UINib(nibName: "TAPreviewCell", bundle: nil), forCellReuseIdentifier: "TAPreviewCell")
        tableView.register(UINib(nibName: "TADescCell", bundle: nil), forCellReuseIdentifier: "TADescCell")
        tableView.register(UINib(nibName: "TADetailsCell", bundle: nil), forCellReuseIdentifier: "TADetailsCell")
        
        bindData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func bindData(){
        
        viewModel.items.bind(to: tableView.rx.items) { [weak self] (tv,row,item) -> UITableViewCell in
            
            if row == 0 {
                let cell = tv.dequeueReusableCell(withIdentifier: "TAPreviewCell", for: IndexPath.init(row: row, section: 0)) as! TAPreviewCell
                cell.previewImage(for: #imageLiteral(resourceName: "ta_example"))
                cell.backButt.rx.tap.bind { [weak self] in
                    self?.navigationController?.popViewController(animated: true)
                    print("back")
                }
                return cell
            }
            else if row == 1 {
                let cell = tv.dequeueReusableCell(withIdentifier: "TADescCell", for: IndexPath.init(row: row, section: 0)) as! TADescCell
                cell.configure(title: "Exodus Travels", desc: "We want to make sure that we can help you to find the perfect adventure holiday. If you have any questions about anything, please get in touch with one of our experts using our Contact Form or by simply calling us.")
                return cell
            }
            else {
                let cell = tv.dequeueReusableCell(withIdentifier: "TADetailsCell", for: IndexPath.init(row: row, section: 0)) as! TADetailsCell
                cell.configure()
                cell.checkOffersButt.rx.tap.bind { [weak self] in
                    print("check Offer")
                    
                    
                    // SEARCH VIEW WITH SELECTED TRAVEL AGENCY
                                        
                }
                cell.callButt.rx.tap.bind { [weak self] in
                    print("Phone")
                }
                cell.emailButt.rx.tap.bind { [weak self] in
                    print("Email")
                }
                return cell
            }
        }
        
        
        self.tableView.rx.setDelegate(self)
        .disposed(by: disposeBag)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 { //Main Image
            return UIScreen.main.bounds.height * 0.3
        }else {
            return UITableView.automaticDimension
        }
    }
    
    
}
