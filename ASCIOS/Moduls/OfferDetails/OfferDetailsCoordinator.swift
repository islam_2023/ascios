//
//  OfferDetailsCoordinator.swift
//  ASCIOS
//
//  Created by islam on 2/17/20.
//  Copyright © 2020 islam. All rights reserved.
//

import Foundation
import RxSwift
import UIKit


class OfferDetailsCoordinator: BaseCoordinator<Void> {
    
    private let navigationController: UINavigationController
    private let dependencies : Dependencies
    
    init(navigationController: UINavigationController,dependencies : Dependencies) {
        self.navigationController = navigationController
        self.dependencies = dependencies
    }
    
    override func start() -> Observable<Void> {
      
        return Observable.empty()
    }
    

}
