//
//  OfferDetailsCell.swift
//  ASCIOS
//
//  Created by islam on 2/17/20.
//  Copyright © 2020 islam. All rights reserved.
//

import UIKit

class OfferDetailsCell: UITableViewCell {

    @IBOutlet weak var flightsLabel: UILabel!
    @IBOutlet weak var flightDgree: UILabel!
    @IBOutlet weak var hotelLabel: UILabel!
    @IBOutlet weak var hotelInfo: UILabel!
    @IBOutlet weak var travelAgencyLabel: UILabel!
    @IBOutlet weak var tarvelAgencyName: UILabel!
    @IBOutlet weak var DetailsLabel: UILabel!
    @IBOutlet weak var hotelDetails: UILabel!
    @IBOutlet weak var flightDetails: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
