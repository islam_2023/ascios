//
//  OfferImageSliderCell.swift
//  ASCIOS
//
//  Created by islam on 2/17/20.
//  Copyright © 2020 islam. All rights reserved.
//

import UIKit
import ImageSlideshow

class OfferImageSliderCell: UITableViewCell {

    @IBOutlet weak var goToLabel: UILabel!
    @IBOutlet weak var locationGoTo: UILabel!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var LocationFrom: UILabel!
    @IBOutlet weak var imageSlider: ImageSlideshow!
    @IBOutlet weak var backButton: UIButton!
    let localSource = [BundleImageSource(imageString: "photo") , BundleImageSource(imageString: "photo") ]

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureSlider ()
        
    }
    func configureSlider (){
            imageSlider.slideshowInterval = 5.0
            imageSlider.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
            imageSlider.contentScaleMode = UIViewContentMode.scaleAspectFill
            imageSlider.pageIndicator = nil
            imageSlider.delegate = self
            // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
           imageSlider.setImageInputs(localSource )
            let recognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
            imageSlider.addGestureRecognizer(recognizer)
    }
    @objc func didTap() {
     
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}

extension OfferImageSliderCell: ImageSlideshowDelegate {
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        print("current page:", page)
    }
}
