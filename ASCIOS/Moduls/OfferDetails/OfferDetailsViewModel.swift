//
//  OfferDetailsViewModel.swift
//  ASCIOS
//
//  Created by islam on 2/17/20.
//  Copyright © 2020 islam. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class OfferDetailsViewModel {
    
    var id : String = ""
    var item = Observable.just([OfferDetailsUIEnum.slider , OfferDetailsUIEnum.description , OfferDetailsUIEnum.details])
    
    let dependency:Dependencies
     
     init(dependency:Dependencies) {
         self.dependency = dependency
     }
    
    
}

enum OfferDetailsUIEnum {
    case slider
    case description
    case details
}
