//
//  OfferDetailsViewController.swift
//  ASCIOS
//
//  Created by islam on 2/17/20.
//  Copyright © 2020 islam. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class OfferDetailsViewController: BaseViewController {

    var viewModel : OfferDetailsViewModel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView,willDecelerate decelerate: Bool){
        if scrollView == tableView {
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                self.bottomView.isHidden = false
            }
            else {
                self.bottomView.isHidden = true
            }
        }
    }

    override func setupUI() {
        bottomView.translatesAutoresizingMaskIntoConstraints = false
           let bottomConstraint = bottomView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
           let widthConstraint = bottomView.widthAnchor.constraint(equalToConstant: self.view.frame.width)
           let heightConstraint = bottomView.heightAnchor.constraint(equalToConstant: 60)
           NSLayoutConstraint.activate([bottomConstraint, widthConstraint, heightConstraint])
         tableView.register(UINib(nibName: "OfferImageSliderCell", bundle: nil), forCellReuseIdentifier: "OfferImageSliderCell")
         tableView.register(UINib(nibName: "OfferDescriptionCell", bundle: nil), forCellReuseIdentifier: "OfferDescriptionCell")
         tableView.register(UINib(nibName: "OfferDetailsCell", bundle: nil), forCellReuseIdentifier: "OfferDetailsCell")
         
         bindData()
     }
    
    
    
    func bindData(){
        viewModel.item.bind(to: tableView.rx.items) { [weak self] (tv,row,item) -> UITableViewCell in
            
            if row == 0 {
                let cell = tv.dequeueReusableCell(withIdentifier: "OfferImageSliderCell", for: IndexPath.init(row: row, section: 0)) as! OfferImageSliderCell
                cell.backButton.rx.tap.bind {[weak self] in
                    self?.navigationController?.popViewController(animated: true)
                    print("back")
                }
                return cell
            }
            else if row == 1 {
                let cell = tv.dequeueReusableCell(withIdentifier: "OfferDescriptionCell", for: IndexPath.init(row: row, section: 0)) as! OfferDescriptionCell
                
                return cell
            }
            else {
                let cell = tv.dequeueReusableCell(withIdentifier: "OfferDetailsCell", for: IndexPath.init(row: row, section: 0)) as! OfferDetailsCell

                return cell
            }
        }
    }
}
