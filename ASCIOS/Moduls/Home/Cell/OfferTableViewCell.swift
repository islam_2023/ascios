//
//  OfferTableViewCell.swift
//  ASCIOS
//
//  Created by islam on 2/16/20.
//  Copyright © 2020 islam. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Kingfisher
class OfferTableViewCell: UITableViewCell {

    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var offerImage: UIImageView!
    @IBOutlet weak var offerTitle: UILabel!
    @IBOutlet weak var offerPrice: UILabel!
    @IBOutlet weak var offerTime: UILabel!
    var favorite = BehaviorRelay(value: false)
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func favoriteButtonAction(_ sender: Any) {
        favorite.accept(!favorite.value)
    }
    
    func configure (offer : OfferModel){
        self.offerTime.text = offer.date_from
        self.offerTitle.text = offer.title
        self.offerPrice.text = String(offer.price ?? 0)
        guard let image = offer.image else {
            return
        }
        if let url = URL(string: image ){
            offerImage.kf.setImage(with: url, placeholder: UIImage(named: "photo"))
        }
        
        
    }
    
}
