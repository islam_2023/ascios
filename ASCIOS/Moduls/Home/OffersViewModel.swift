//
//  OffersViewModel.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/13/20.
//  Copyright © 2020 islam. All rights reserved.
//

import Foundation
import CoreData
import RxSwift
import RxCocoa
class OfferViewModel : BaseViewModel {
    
    
    let dependency:Dependencies
    var managedObjectContext: NSManagedObjectContext!
    
    let isLoading: ActivityIndicator =  ActivityIndicator()
    
    var getOffersData: Observable<APIResult<GetOffersResponse>> {
        return _getOffersData.asObservable().observeOn(MainScheduler.instance)
    }
    private let _getOffersData = ReplaySubject<APIResult<GetOffersResponse>>.create(bufferSize: 1)

    //Data
    var OffersTableData: Observable<[OfferModel]>
    var Offers: Variable<[OfferModel]> = Variable([])
    
    //Paging Metadata
    var nextPage: Int? = 1
    var isFromCoredata: Bool = false
    
    //Method
    var callNextPage = PublishSubject<Void>()
    lazy var selectedItem = PublishSubject<OfferModel?>()
    
    let items = Observable.just(
        (0..<20).map { "\($0)" }
    )
    
    init(dependency:Dependencies) {
        self.dependency = dependency
        self.OffersTableData = Offers.asObservable()
        self.managedObjectContext = dependency.managedObjectContext
        super.init()
        self.callNextPage.asObservable().subscribe(onNext: { [weak self] in
            guard let `self` = self else { return }
            // Check internet availability, call next page API if internet available
            if UtilityFunctions.isConnectedToInternet == true {
                if self.nextPage != nil {
                    self.getOffers(nextPage: self.nextPage!)
                }
            } else {
                // Fetch movie data from local cache, as internet is not available
            //    self.getMoviesFromCoreData()
            }
        }).disposed(by: disposeBag)
        
        getOffersData
            .subscribe(onNext: { [weak self] (result) in
                guard let `self` = self else { return }
                switch result {
                case .success(let response):
                    if let data = response.data  {
                        if let _offers = data.data_list{
                            for offer in _offers {
                                _ = try? self.managedObjectContext.rx.update(OffersCoredataModel.init(offer: offer))
                            }
                            self.Offers.value.append(contentsOf: _offers)
                        }
                    }
                    self.nextPage = response.nextPage
                default:
                    break
                }
            })
            .disposed(by: disposeBag)
    }
    
    
}
//MARK:- Core Data
extension OfferViewModel {
    
    //    func getOffersFromCoreData() {
    //
    //        isFromCoredata = true
    //        managedObjectContext.rx.entities(MoviesCoredataModel.self, sortDescriptors: []).asObservable()
    //            .subscribe(onNext: { [weak self] movieModels in
    //                guard let `self` = self else {return}
    //
    //                // Check local cache record count and binded array count same, no need to execute further code
    //                if self.movies.value.count == movieModels.count {
    //                    return
    //                }
    //
    //                var movies = [Movie]()
    //                for movie in movieModels {
    //                    let movieModel = Movie.init(model: movie)
    //                    // Check movie object is contains in main array which bind to tableview, ignore that object
    //                    if self.movies.value.contains(where: { $0.id == movieModel.id }) == false {
    //                        movies.append(Movie.init(model: movie))
    //                    }
    //                }
    //
    //                self.movies.value.append(contentsOf: movies)
    //                self.nextPage = (self.movies.value.count/20)+1
    //            }).disposed(by: disposeBag)
    //    }
    
}

//MARK:- API Call
extension OfferViewModel {
    
    func getOffers(nextPage: Int = 1) {
        
        dependency.api.getOffersList(page: String(nextPage), destination: nil , month : nil , travel_agency : nil)
            .trackActivity(nextPage == 1 ? isLoading : ActivityIndicator())
            .observeOn(SerialDispatchQueueScheduler(qos: .default))
            .subscribe {[weak self] (event) in
                guard let `self` = self else { return }
                switch event {
                case .next(let result):
                    switch result {
                    case .success:
                        self._getOffersData.on(event)
                    case .failure(let error):
                        // Fetch data from local cache when internet is not available
                        if error.code == InternetConnectionErrorCode.offline.rawValue {
                            // self.getMoviesFromCoreData()
                            self.alertDialog.onNext((NSLocalizedString("Network error", comment: ""), error.message))
                        } else {
                            self.alertDialog.onNext(("Error", error.message))
                        }
                    }
                    
                default:
                    break
                }
        }.disposed(by: disposeBag)
    }
    
}
