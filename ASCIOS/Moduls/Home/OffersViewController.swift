//
//  HomeViewController.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/13/20.
//  Copyright © 2020 islam. All rights reserved.
//

import UIKit
import RxSwift

class OffersViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let offerTableViewCell = "OfferTableViewCell"
    var offerViewModel : OfferViewModel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        offerViewModel.getOffers()
        // Do any additional setup after loading the view.
    }
    
    override func setupUI()  {
        self.title = "Explore Cities"
        self.tabBarController?.tabBarItem.title = nil
        tableView.register(UINib(nibName: offerTableViewCell, bundle: nil), forCellReuseIdentifier: offerTableViewCell)
        
        offerViewModel.OffersTableData
            .bind(to: tableView.rx.items(cellIdentifier: offerTableViewCell, cellType: OfferTableViewCell.self)) { [weak self ](row, element, cell) in
                
                cell.configure(offer: element)
                
                cell.favorite.subscribe({[weak self] item  in
                 
                })
                
            }
            .disposed(by: disposeBag)

        tableView.rx.modelSelected(OfferModel.self).bind(to: offerViewModel.selectedItem).disposed(by: disposeBag)
        
        tableView.rx
            .willDisplayCell
            .filter({[weak self] (cell, indexPath) in
                guard let `self` = self else { return false }
                return (indexPath.row + 1) == self.tableView.numberOfRows(inSection: indexPath.section) - 3
            })
            .throttle(1.0, scheduler: MainScheduler.instance)
            .map({ event -> Void in
                return Void()
            })
            .bind(to: offerViewModel.callNextPage)
            .disposed(by: disposeBag)

        offerViewModel.isLoading
            .distinctUntilChanged()
            .drive(onNext: { [weak self] (isLoading) in
                guard let `self` = self else { return }
            // Hide Indicator
                if isLoading {
            // Show Indicator
                }
            })
            .disposed(by: disposeBag)
        
        offerViewModel.alertDialog.observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self] (title, message) in
                guard let `self` = self else {return}
                // present alert
            }).disposed(by: disposeBag)
    }
}


