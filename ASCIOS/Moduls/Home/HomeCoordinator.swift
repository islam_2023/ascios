//
//  HomeCoordinator.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/13/20.
//  Copyright © 2020 islam. All rights reserved.
//

import Foundation
import RxSwift
import UIKit


class OffersCoordinator: BaseCoordinator<Void> {
    
    private let navigationController: UINavigationController
    private let dependencies:Dependencies
    
    init(navigationController: UINavigationController ,dependencies:Dependencies) {
        self.navigationController = navigationController
        self.dependencies = dependencies
        
    }
    
    override func start() -> Observable<Void> {
        let viewController = UIStoryboard.main.homeViewController
        let viewModel = OfferViewModel(dependency: self.dependencies)
        viewController.offerViewModel = viewModel
        viewModel.selectedItem.asObservable().subscribe(onNext: { item in
          self.presentToPaymentConfirmtion()
//            self.pushToOfferDetails(item: item)
//            self.pushToTADetails()
            self.pushToBook()
        }).disposed(by: disposeBag)
        
        navigationController.setViewControllers([viewController], animated: true)
        return Observable.empty()
    }
    
    func pushToOfferDetails(item : Any){
        DispatchQueue.main.async {
            let viewModel = OfferDetailsViewModel(dependency: self.dependencies)
            let vc = UIStoryboard.main.offerDetailsViewController
            vc.viewModel = viewModel
            self.navigationController.pushViewController(vc, animated: true)
        }
        
    }
    
    func pushToTADetails(){
        let viewModel = TAViewModel(dependencies: self.dependencies)
        let vc = UIStoryboard.main.TAProfileVC
        vc.viewModel = viewModel
        DispatchQueue.main.async {
            self.navigationController.pushViewController(vc, animated: true)
        }
    }
    func presentToPaymentConfirmtion(){
        let vc = UIStoryboard.AlertScreens.paymentConfirmtionViewController
        DispatchQueue.main.async {
            self.navigationController.present(vc, animated: true, completion: nil)
        }
    }
    
    func pushToBook(){
        let viewModel = BookViewModel(dependencies: self.dependencies)
        let vc = UIStoryboard.main.bookViewController
        vc.viewModel = viewModel
        DispatchQueue.main.async {
            self.navigationController.pushViewController(vc, animated: true)
        }
    }
    
    deinit {
        plog(OffersCoordinator.self)
    }
}
