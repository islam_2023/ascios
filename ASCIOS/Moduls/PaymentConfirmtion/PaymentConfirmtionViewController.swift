//
//  PaymentConfirmtionViewController.swift
//  ASCIOS
//
//  Created by islam on 2/19/20.
//  Copyright © 2020 islam. All rights reserved.
//

import UIKit

class PaymentConfirmtionViewController: UITableViewController {
    
    @IBOutlet weak var confimationImage: UIImageView!
    @IBOutlet weak var confirmMessageTitle: UILabel!
    @IBOutlet weak var confirmMessage: UILabel!
    @IBOutlet weak var transactionLabel: UILabel!
    @IBOutlet weak var tryAgainButton: UIButton!
    @IBOutlet weak var backToHomeButton: UIButton!
    @IBOutlet weak var transactionId: UILabel!
    
    
    var confirmFlag = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI ()
    }
    
    // MARK: - Table view data source
    
    func setupUI (){
        if !confirmFlag {
            confimationImage.image = UIImage(named: "paymentFailed")
            tryAgainButton.isHidden = false
            confirmMessageTitle.text = "Payment Failed"
            confirmMessage.text = "passes. Make a symbolic breakpoint at UITableViewAlertForLayoutOutsideViewHierarchy to catch this in the debugger and see what"
            self.tableView.backgroundColor = UIColor(displayP3Red: 214/225, green: 70/225, blue: 83/225, alpha: 1)
        }
        else {
            confimationImage.image = UIImage(named: "paymentConfirmed")
            tryAgainButton.isHidden = true
            confirmMessageTitle.text = "Payment Confirmed!"
            confirmMessage.text = "passes. Make a symbolic breakpoint at UITableViewAlertForLayoutOutsideViewHierarchy to catch this in the debugger and see what"
            transactionId.text = "763878723"
            self.tableView.backgroundColor = UIColor(displayP3Red: 95/225, green: 172/225, blue: 192/225, alpha: 1)
        }
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !confirmFlag {
            if indexPath.row == 1 {
                return 0
            }
        }
        else{
            if indexPath.row == 1 {
                return 105
            }
        }
        if indexPath.row == 0 {
            return 391
        }
        else {
            return 176
        }
    }
    
    func bindData (){
        
    }
    
    
}
