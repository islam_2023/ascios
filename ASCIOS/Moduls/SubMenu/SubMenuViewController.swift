//
//  SubMenuViewController.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/16/20.
//  Copyright © 2020 islam. All rights reserved.
//

import UIKit
import WebKit

class SubMenuViewController: BaseViewController {

    @IBOutlet weak var contentWebView:WKWebView!
    var viewModel : SettingViewModel?
    var coordinator:SettingCoordinator?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func setupUI() {
        viewModel?.data.asObservable().subscribe(onNext: { [weak self] (data) in
            guard let strongSelf = self else {return}
            strongSelf.contentWebView.loadHTMLString(data, baseURL: nil)
        }).disposed(by: disposeBag)
    }
    
    
}
