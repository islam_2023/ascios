//
//  SettingCoordinator.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/16/20.
//  Copyright © 2020 islam. All rights reserved.
//

import Foundation
import RxSwift
import UIKit


class SettingCoordinator: BaseCoordinator<Void> {
    
    private let navigationController: UINavigationController
    private let dependencies : Dependencies
    
    init(navigationController: UINavigationController,dependencies : Dependencies) {
        self.navigationController = navigationController
        self.dependencies = dependencies
    }
    
    override func start() -> Observable<Void> {
        let viewController = UIStoryboard.main.settingViewController
        navigationController.setViewControllers([viewController], animated: true)
        return Observable.empty()
    }
    
    func pushSubSetting(type:Settings){
        let viewModel = SettingViewModel(dependency: self.dependencies)
        viewModel.settingType = type
        let vc = UIStoryboard.main.subMenuViewController
        vc.viewModel = viewModel
        self.navigationController.pushViewController(vc, animated: true)
    }
}


