//
//  SettingModel.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/16/20.
//  Copyright © 2020 islam. All rights reserved.
//

import Foundation
import UIKit


enum Settings :Int{
    case about = 0
    case contact = 1
    case terms = 2
    case privacy = 3
}

class SettingModel {
    
    var title:String
    var image:UIImage
    var selectedSetting = false
    
    init(title:String,image:UIImage) {
        self.title = title
        self.image = image
    }
    
    
    
    
}
