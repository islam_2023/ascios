//
//  SettingTCell.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/16/20.
//  Copyright © 2020 islam. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class SettingTCell: UITableViewCell {

    
    @IBOutlet weak var mImgV:UIImageView!
    @IBOutlet weak var mTitleLbl:UILabel!
    
    @IBOutlet weak var butt:UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.mImgV?.image = nil
    }
    
    func configure(image:UIImage,title:String) {
        self.mImgV.image = image
        self.mTitleLbl.text = title
    }
    
}
