//
//  SettingViewController.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/16/20.
//  Copyright © 2020 islam. All rights reserved.
//

import UIKit

class SettingViewController: BaseViewController {

    @IBOutlet weak var tableView:UITableView!
    
    var settingItems = [SettingModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func setupUI() {
        self.title = "Menu"
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "SettingTCell", bundle: nil), forCellReuseIdentifier: "SettingTCell")
        
        let about = SettingModel(title: L.about.Locaz, image: #imageLiteral(resourceName: "test"))
        let contact = SettingModel(title: L.contact.Locaz, image:  #imageLiteral(resourceName: "test"))
        let terms = SettingModel(title: L.terms.Locaz, image:  #imageLiteral(resourceName: "test"))
        let privacy = SettingModel(title: L.privacy.Locaz, image:  #imageLiteral(resourceName: "test"))
        self.settingItems = [about,contact,terms,privacy]
    }
    
}

extension SettingViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingTCell", for: indexPath) as! SettingTCell
        let obj = self.settingItems[indexPath.row]
        cell.configure(image: obj.image, title: obj.title)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
}

extension SettingViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        self.settingItems.map { (setting) in
//            setting.selectedSetting = false
//        }
//        self.settingItems[indexPath.row].selectedSetting = true
        
        switch indexPath.row {
        case Settings.about.rawValue:
            print(L.about.Locaz)
        case Settings.contact.rawValue:
            print(L.contact.Locaz)
        case Settings.terms.rawValue:
            print(L.terms.Locaz)
        case Settings.privacy.rawValue:
            print(L.privacy.Locaz)
        default :break
        }
    }
}
