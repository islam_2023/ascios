//
//  SettingViewModel.swift
//  ASCIOS
//
//  Created by Emad Habib on 2/16/20.
//  Copyright © 2020 islam. All rights reserved.
//

import Foundation
import RxSwift

class SettingViewModel {
    
    let dependency:Dependencies
    
    var data = Variable("")
    var settingType :Settings = .about

    init(dependency:Dependencies) {
        self.dependency = dependency
        
    }
    
    func getSettingData() {
        switch settingType {
        case .about:()
            //GetAboutData
        case .contact:()
            //GetContactData
        case .terms:()
            //GetTermsData
        case .privacy:()
            //GetPrivacyData
        }
    }
    
}
