//
//  APIRouter.swift
//  MovieApp
//
//  Created by Anshul Shah on 12/11/18.
//  Copyright © 2018 Anshul Shah. All rights reserved.
//

import Foundation
import Alamofire


enum APIRouter:URLRequestConvertible {

    case getAllOffers(page: String, destination: String? , month : String? , travel_agency : String?)
    
    
    func asURLRequest() throws -> URLRequest {
        
        var method: HTTPMethod {
            switch self {
            case .getAllOffers:
                return .get
            }
        }
        
        let params: ([String: Any]?) = {
            switch self {
            case .getAllOffers:
                return nil

            }
        }()
        
        let url: URL = {
            // Add base url for the request
            let baseURL:String = {
                return Environment.APIBasePath()
            }()
            // build up and return the URL for each endpoint
            let relativePath: String? = {
                switch self {
                case .getAllOffers(let page, let destination,let  month , let travel_agency):
                  //  return "getoffers?destination=\(String(describing: destination))&month=\(String(describing: month))&travel_agency=\(String(describing: travel_agency))&page=\(page)"
                    return "getoffers?destination=0&month=2&travel_agency=1&page=1"
                }
            }()
            
            var url = URL(string: baseURL)!
//            if let relativePath = relativePath {
//                url = url.appendingPathComponent(relativePath)
//            }
            return url
        }()
        
        let encoding:ParameterEncoding = {
            return URLEncoding.default
        }()
        
        let headers:[String:String]? = {
            // AUTH Header
         let header =   ["Content-Type" : "application/json" , "lang": "ar" , "country_id":"1" , "Accept" : "application/json"]
            return header
        }()
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        urlRequest.allHTTPHeaderFields = headers

        return try encoding.encode(urlRequest, with: params)
    }
}
