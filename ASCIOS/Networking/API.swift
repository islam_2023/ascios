//
//  API.swift
//  MovieApp
//
//  Created by Anshul Shah on 12/11/18.
//  Copyright © 2018 Anshul Shah. All rights reserved.
//

import Foundation
import RxSwift
import RxAlamofire
import Alamofire

class API {
    
    static let shared:API = {
        let instance = API()
        return instance
    }()
    
    init() {
        
    }
    
    // Call Get Offers List API
    func getOffersList(page: String, destination: String? , month : String? , travel_agency : String?) -> Observable<APIResult<GetOffersResponse>> {

        return API.handleDataRequest(dataRequest: APIManager.shared.requestObservable(api: APIRouter.getAllOffers(page: page, destination: destination , month : month , travel_agency : travel_agency))).map({ (response) -> APIResult<GetOffersResponse> in
            if (response ?? [:]).keys.contains("Error") {
                if (response ?? [:]).keys.contains("IsInternetOff") {
                    if let isInternetOff = response!["IsInternetOff"] as? Bool {
                        if isInternetOff {
                            return APIResult.failure(error: APICallError(critical: false, code: InternetConnectionErrorCode.offline.rawValue, reason: response!["Error"] as! String, message: response!["Error"] as! String))
                        }
                    }
                }
                return APIResult.failure(error: APICallError(critical: false, code: 1111, reason: response!["Error"] as! String, message: response!["Error"] as! String))
            }

            let apiResponse = GetOffersResponse(response: response)
            let (apiStatus, _) = (true, APICallError.init(status: .success))
            if apiStatus { return APIResult.success(value: apiResponse) }
        })
    }

    
    
    
    private static func handleDataRequest(dataRequest: Observable<DataRequest>) -> Observable<[String:Any]?> {

        if NetworkReachabilityManager()!.isReachable == false {
            return Observable<[String: Any]?>.create({ (observer) -> Disposable in
                observer.on(.next(["Error":"Unable to contact the server".localized , "IsInternetOff":true]))
                observer.on(.completed)
                return Disposables.create()
            })
        }
        
        return Observable<[String: Any]?>.create({ (observer) -> Disposable in
            dataRequest.observeOn(MainScheduler.instance).subscribe({ (event) in
                
                switch event {
                
                case .next(let e):
                    plog(e.debugDescription)
                    
                    e.responseJSON(completionHandler: { (dataResponse) in
                        
                        switch dataResponse.result {
                            
                        case .success(let data):
                            
                            guard let json = data as? [String:Any] else {
                                observer.onNext(nil)
                                return
                            }
                            
                            plog("HEADER RESPONSE CODE : \(dataResponse.response!.statusCode)")
                            observer.onNext(json)
                            
                        case .failure(let error):
                            plog(error)
                            let errorCode = (error as NSError).code
                            if errorCode == -1005 || errorCode == -1009 {
                                observer.onNext(["Error": NSLocalizedString("Unable to contact the server", comment: ""),
                                                 "IsInternetOff":true])
                            } else {
                                observer.onNext(["Error":error.localizedDescription,
                                                 "IsInternetOff":false])
                            }
                            observer.onCompleted()
                        }
                    })
                case .error(let error):
                    plog(error)
                    observer.onNext(["Error":error.localizedDescription])
                    observer.onNext(nil)
                    
                case .completed:
                    observer.onCompleted()
                }
            })
        })
    }
}
