

import Foundation
import CoreData
import RxDataSources
import RxCoreData

struct OffersCoredataModel {
    

    var id : String?
    var cityId : Int?
    var cityName : String?
    var images : [String]?
    var image : String?
    var description : String?
    var title : String?
    var price : Int32?
    var travelAgencyId : Int32?
    var currency : String?
    var favoritesCount : Int32?
    var date_from : String?
    var date_to : String?
    var isfavorite : Bool

    
    
    
    
    init(offer: OfferModel) {
        self.id = offer.id ?? ""
        self.images = offer.images ?? []
        self.title = offer.title ?? ""
        self.travelAgencyId = Int32(offer.travelAgencyId ?? 0)
        self.description = offer.description ?? ""
        self.currency = offer.currency
        self.isfavorite = false
        self.price = Int32(offer.price ?? 0)
        self.image = offer.image ?? ""
        self.date_to = offer.date_to
        self.date_from = offer.date_from
        self.favoritesCount = Int32(offer.favoritesCount ?? 0)
        
    }
}

func == (lhs: OffersCoredataModel, rhs: OffersCoredataModel) -> Bool {
    return lhs.id == rhs.id
}

extension OffersCoredataModel : Equatable { }

extension OffersCoredataModel : IdentifiableType {
    typealias Identity = String
    
    var identity: Identity { return "\(id)" }
}

extension OffersCoredataModel : Persistable {
    typealias T = NSManagedObject
    
    static var entityName: String {
        return "Offers"
    }
    
    static var primaryAttributeName: String {
        return "id"
    }
    
    init(entity: T) {
        
        
        id = entity.value(forKey: "id") as! String
        title = entity.value(forKey: "title") as! String
        image = entity.value(forKey: "image") as! String
        description = entity.value(forKey: "descriptions") as! String
        price = entity.value(forKey: "price") as! Int32
        travelAgencyId = entity.value(forKey: "travelAID") as! Int32
        images = entity.value(forKey: "images") as! [String]
        isfavorite = entity.value(forKey: "isfavorite") as! Bool
        currency = entity.value(forKey: "currency") as! String
        date_from = entity.value(forKey: "date_from") as! String
        date_to = entity.value(forKey: "date_to") as! String
        
    }

    func update(_ entity: T) {
        
        entity.setValue(currency, forKey: "currency")
        entity.setValue(date_from, forKey: "date_from")
        entity.setValue(date_to, forKey: "date_to")
        entity.setValue(id, forKey: "id")
        entity.setValue(title, forKey: "title")
        entity.setValue(price, forKey: "price")
        entity.setValue(description, forKey: "descriptions")
        entity.setValue(images, forKey: "images")
        entity.setValue(image, forKey: "image")
        entity.setValue(isfavorite, forKey: "isfavorite")
        entity.setValue(travelAgencyId, forKey: "travelAID")

        do {
            try entity.managedObjectContext?.save()
        } catch let e {
            print(e)
        }
    }
    
}
