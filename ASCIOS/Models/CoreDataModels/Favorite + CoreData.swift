//
//  Favorite + CoreData.swift
//  ASCIOS
//
//  Created by islam on 2/13/20.
//  Copyright © 2020 islam. All rights reserved.
//

import Foundation
import CoreData
import RxDataSources
import RxCoreData

struct FavoriteCoredataModel {

    var offers : [OfferModel]
    var id : Int32
    init(offers: [OfferModel]) {
        self.offers = offers
        self.id = 0
    }
}

//offersItems



extension FavoriteCoredataModel : Equatable {
    static func == (lhs: FavoriteCoredataModel, rhs: FavoriteCoredataModel) -> Bool {
        return false
    }
}

extension FavoriteCoredataModel : IdentifiableType {
    var identity: String {
        "\(id)"
    }
    typealias Identity = String
    
}

extension FavoriteCoredataModel : Persistable {
    static var primaryAttributeName: String {
        "id"
    }

    typealias T = NSManagedObject
    
    static var entityName: String {
        return "FavoriteOffers"
    }
    
    init(entity: T) {
        offers = entity.value(forKey: "offersItems") as! [OfferModel]
        id = entity.value(forKey: "id") as! Int32
    }

    func update(_ entity: T) {
        entity.setValue(offers, forKey: "offersItems")
        entity.setValue(id, forKey: "id")


        do {
            try entity.managedObjectContext?.save()
        } catch let e {
            print(e)
        }
    }
    
}
