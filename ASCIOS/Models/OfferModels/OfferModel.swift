//
//  OfferModel.swift
//  ASCIOS
//
//  Created by islam on 2/13/20.
//  Copyright © 2020 islam. All rights reserved.
//

import Foundation

struct OfferModel:Codable {
    var id : String?
    var cityId : Int?
    var cityName : String?
    var images : [String]?
    var image : String?
    var description : String?
    var title : String?
    var price : Int?
    var travelAgencyId : Int?
    var currency : String?
    var favoritesCount : Int?
    var date_from : String?
    var date_to : String?
    private enum CodingKeys: String, CodingKey {
        case  id, cityId = "city_id" , cityName = "city_name" , image ,title , price = "pricy" , currency , favoritesCount = "favorites_count" , date_from ,date_to
    }
    
    init(offer: OffersCoredataModel) {
        self.id = offer.id
        self.images = offer.images
        self.title = offer.title
        self.travelAgencyId = Int(offer.travelAgencyId ?? 0)
        self.description = offer.description
        self.price = Int(offer.price ?? 0)
        self.image = offer.image
        self.currency = offer.currency
        self.date_from = offer.date_from
        self.date_to = offer.date_to
    }
    
}

struct OffersResponse:Codable {
    var data_list : [OfferModel]?
    var current_page : String?
    var total_count : String?
    var page_size : Int?
    
//    private enum CodingKeys: String, CodingKey {
//        case offerList = "data_list" , current_page , total_count , page_size
//    }
    
}

struct GetOffersResponse:Codable {
    var data : OffersResponse?
    var status : Int?
    var message : String?
    var nextPage: Int?
    
    init(response: [String: Any]?) {
        guard let response = response else {
            return
        }
        if let offersData = try? JSONSerialization.data(withJSONObject: response, options: []) {
            if let offersResponse = try? JSONDecoder().decode(GetOffersResponse.self, from: offersData) {
                self.data = offersResponse.data
                self.status = offersResponse.status
                self.message = offersResponse.message
                guard let _page = offersResponse.data?.current_page else {
                    self.nextPage = nil
                    return
                }
                guard let _totalCount = offersResponse.data?.total_count else{
                    self.nextPage = nil
                    return
                }
                if (Int(_page)! * 10) > Int(_totalCount)! {
                    self.nextPage = nil
                    
                }
                self.nextPage =  Int(_page)! + 1
            }
        }
    }
    
}

