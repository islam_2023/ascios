//
//  AppCoordinator.swift
//  MovieApp
//
//  Created by Anshul Shah on 11/12/18.
//  Copyright © 2018 Anshul Shah. All rights reserved.
//

import UIKit
import RxSwift
import CoreData

class AppCoordinator: BaseCoordinator<Void> {
    
    private let window: UIWindow
    private let  dependencies: Dependencies
    init(window: UIWindow) {
        self.window = window
        self.window.backgroundColor = UIColor.white
        self.dependencies = AppDependency(window: window, managedContext: Application.managedObjectContext)
    }
    
    override func start() -> Observable<CoordinationResult> {
        let homeCoordinator = HomeCoordinator(window: window,dependencies: dependencies)
        return self.coordinate(to: homeCoordinator)
    }
}


class HomeCoordinator: BaseCoordinator<Void> {
    
    private let window: UIWindow
    private let dependencies: Dependencies
    private let viewControllers: [UINavigationController]
    
    init(window: UIWindow,dependencies: Dependencies) {
        self.dependencies = dependencies
        self.window = window
        self.viewControllers = HomeBarKind.items
            .map({ (items) -> UINavigationController in
                let navigation = UINavigationController()
                navigation.tabBarItem.title = items.title
                navigation.tabBarItem.image = items.icon

                if #available(iOS 13.0, *) {
                    let appearance = UINavigationBarAppearance()
                    appearance.configureWithTransparentBackground()
                    navigation.navigationBar.scrollEdgeAppearance = appearance
                    navigation.navigationBar.compactAppearance = appearance
                    navigation.navigationBar.standardAppearance = appearance
                    navigation.navigationBar.prefersLargeTitles = true
                }

                return navigation
            })
    }
    
    
    func setNavStyle(){
        
        
        

    }
    
    override func start() -> Observable<CoordinationResult> {
        let viewController = UITabBarController()
        viewController.tabBar.isTranslucent = false
        viewController.viewControllers = viewControllers
        
        
        let coordinates = viewControllers.enumerated()
            .map { (offset, element) -> Observable<Void> in
                guard let items = HomeBarKind(rawValue: offset) else { return Observable.just(() )}
                switch items {
                case .offers:
                    return coordinate(to: OffersCoordinator(navigationController: element, dependencies: self.dependencies))
                case .search:
                    return coordinate(to: OffersCoordinator(navigationController: element, dependencies: self.dependencies))
                case .favs:
                    return coordinate(to: OffersCoordinator(navigationController: element, dependencies: self.dependencies))
                case .more:
                    return coordinate(to: SettingCoordinator(navigationController: element, dependencies: self.dependencies))
                @unknown default:
                    return coordinate(to: OffersCoordinator(navigationController: element, dependencies: self.dependencies))
                }
        }
        
        
        //LcalizeApp
        
        Localizer.setLang()
        
        Observable.merge(coordinates)
            .subscribe()
            .disposed(by: disposeBag)
        
        window.rootViewController = viewController
        window.makeKeyAndVisible()
        return Observable.never()
    }
}

